-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 06, 2022 at 01:41 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qlsv`
--

-- --------------------------------------------------------

--
-- Table structure for table `dmkhoa`
--

CREATE TABLE `dmkhoa` (
  `MaKH` varchar(6) NOT NULL,
  `TenKhoa` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dmkhoa`
--

INSERT INTO `dmkhoa` (`MaKH`, `TenKhoa`) VALUES
('CNTT', 'Công nghệ thông tin'),
('HOA', 'Hóa học'),
('SINH', 'Sinh học');

-- --------------------------------------------------------

--
-- Table structure for table `sinhvien`
--

CREATE TABLE `sinhvien` (
  `MaSV` varchar(6) NOT NULL,
  `HoSV` varchar(30) DEFAULT NULL,
  `TenSV` varchar(15) DEFAULT NULL,
  `GioiTinh` char(1) DEFAULT NULL,
  `NgaySinh` datetime DEFAULT NULL,
  `NoiSinh` varchar(50) DEFAULT NULL,
  `DiaChi` varchar(50) DEFAULT NULL,
  `MaKH` varchar(6) DEFAULT NULL,
  `HocBong` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sinhvien`
--

INSERT INTO `sinhvien` (`MaSV`, `HoSV`, `TenSV`, `GioiTinh`, `NgaySinh`, `NoiSinh`, `DiaChi`, `MaKH`, `HocBong`) VALUES
('1', 'Nguyen', 'Son', 'M', '2001-09-08 00:00:00', 'Hanoi', 'Hanoi', 'CNTT', 1),
('10', 'Trung', 'Hao', 'M', '1997-09-15 00:00:00', 'Haiphong', 'VN', 'CNTT', 0),
('2', 'Le', 'Thanh', 'M', '1995-12-08 00:00:00', 'HCM', 'Hanoi', 'CNTT', 2),
('3', 'Doo', 'Chung', 'M', '1990-02-08 00:00:00', 'Danang', 'VN', 'SINH', 3),
('4', 'Dinh Nho', 'Hao', 'M', '1997-09-15 00:00:00', 'Haiphong', 'VN', 'HOA', 0),
('5', 'Tran', 'Hoang', 'M', '2001-09-08 00:00:00', 'Hanoi', 'Hanoi', 'CNTT', 1),
('6', 'Le', 'Trang', 'M', '1995-12-08 00:00:00', 'HCM', 'Hanoi', 'CNTT', 2),
('7', 'Doo', 'Chung', 'M', '1990-02-08 00:00:00', 'Danang', 'VN', 'SINH', 3),
('8', 'Cuong', 'Hao', 'M', '1997-09-15 00:00:00', 'Haiphong', 'VN', 'HOA', 0),
('9', 'Nhat', 'Chung', 'M', '1990-02-08 00:00:00', 'Danang', 'VN', 'SINH', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dmkhoa`
--
ALTER TABLE `dmkhoa`
  ADD PRIMARY KEY (`MaKH`);

--
-- Indexes for table `sinhvien`
--
ALTER TABLE `sinhvien`
  ADD PRIMARY KEY (`MaSV`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
